<?php
/**
 * Author: Wesley
 * Date: 9/5/13
 * Time: 2:13 PM
 */

return array(

    /*
    |--------------------------------------------------------------------------
    | Your AWS Credentials
    |--------------------------------------------------------------------------
    |
    | For more details about configuration setup, see the following URL:
    | https://github.com/ScientiaMobile/laravel-streamer/blob/master/src
    | /config/config.php
    |
    */
       
    'aws' => array (
        'key'    => 'AKIAI6WOUZ3BG4LRJ47Q',
        'secret' => 'zzzlLS0IF6NPmBrovmL6DvR8I5ECRs3T7lw6J2Pr',
        //'key'    => 'YOUR_AWS_ACCESS_KEY_ID',
        //'secret' => 'YOUR_AWS_SECRET_KEY',
        'region' => 'us-east-1',
        'config_file' => null,
    ),

    /*
    |--------------------------------------------------------------------------
    | Views
    |--------------------------------------------------------------------------
    |
    | The default views that the file Smuploader will use.
    |
    */

    'smuploader_form' => 'smuploader::upload-file',


    /*
    |--------------------------------------------------------------------------
    | Model
    |--------------------------------------------------------------------------
    |
    | Model the Smuploader will use.
    | Default : Smuploader
    |
    */

    'upload_model' => 'Smuploader',


    /*
    |--------------------------------------------------------------------------
    | Table
    |--------------------------------------------------------------------------
    |
    | Table the Smuploader will use.
    | Default : uploads
    |
    */

    'upload_table' => 'sm-uploads',

    /*
    |--------------------------------------------------------------------------
    | Upload Files
    |--------------------------------------------------------------------------
    |
    | Configuration items for uploaded files.
    |
    */
    'upload_file_types' => array('image/png','image/gif','image/jpg','image/jpeg'),
    'upload_file_extensions' => array('png','gif','jpg','jpeg'), // Case insensitive

    // Max upload size - In BYTES. 1GB = 1073741824 bytes, 10 MB = 10485760, 1 MB = 1048576
    'max_upload_file_size' => 10485760, // Converter - http://www.beesky.com/newsite/bit_byte.htm

    // [True] will change all uploaded file names to an obfuscated name. (Example_Image.jpg becomes Example_Image_p4n8wfnt8nwh5gc7ynwn8gtu4se8u.jpg)
    // [False] attempts to leaves the filename as is.
    //TODO: WES' NOTE: Setting this to TRUE does not yet work.  The $file object is converted to a string if set to true
    //TODO: Will have to fix this at some other point.
    'obfuscate_filenames' => false, // True/False

    /*
    |--------------------------------------------------------------------------
    | Upload Folder
    |--------------------------------------------------------------------------
    |
    | Folder the Smuploader will use.
    | This will need to writable by the web server.
    | Default : public/packages/scientiamobile/smuploader/uploads/
    | Recommendation: public/uploads/
    |
    */
    'upload_folder' => 'public/packages/scientiamobile/smuploader/uploads/',
    'upload_folder_public_path' => 'packages/scientiamobile/smuploader/uploads/',
    'upload_folder_permission_value' => 0777, // Default 0777 - Other likely values 0775, 0755
);