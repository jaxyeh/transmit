<?php namespace ScientiaMobile\MdpUploader;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Uploader extends Eloquent
{

  public static $app;

  /**
   * Get the uploads owner.
   *
   * @return User
   */
  public function user()
  {
    return $this->belongsTo('User', 'user_id');
  }

  /**
   * Get children uploads
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function children()
  {
    return $this->hasMany('Upload', 'parent_id');
  }

  /**
   * Create a new CabinetUpload instance.
   */
  public function __construct( array $attributes = array() )
  {
      parent::__construct( $attributes );

      if ( ! static::$app )
          static::$app = app();

      // Since App is statically loaded, you can retrieve the config file
      //dd(static::$app['config']->get('mdp-uploader::aws'));
  }


/*****************/

  public function getSignedUrl($bucket, $key) {
    // Get Signed URL
    $s3 = static::$app->make('S3Uploader')->getS3Client();
    $signed = $s3->getObjectUrl($bucket, $key, '+10 minutes');

    // Save to database?

    // Return Authentication key
    dd($signed);
  }

  public function process(UploadFile $file) {

    $uploader = static::$app->make('S3Uploader');
    // - OR -
    //$uploader = static::$app->make('FileUploader');

    // File extension
    $this->extension = $file->getClientOriginalExtension();
    // Mimetype for the file
    $this->mimetype = $file->getMimeType();
    // Current user or 0
    $this->user_id = (Auth::user() ? Auth::user()->id : 0);

    $this->size = $file->getSize();

    list($this->path, $this->filename) = $uploader->upload($file);

    if (!$this->save()) {
        // Error Exception
    }
  }

}