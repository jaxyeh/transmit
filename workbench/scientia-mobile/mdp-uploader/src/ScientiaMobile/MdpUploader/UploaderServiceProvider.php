<?php namespace ScientiaMobile\MdpUploader;

use Illuminate\Support\ServiceProvider;
use ScientiaMobile\MdpUploader\Adapters\S3Uploader;
use ScientiaMobile\MdpUploader\Adapters\FileUploader;

class UploaderServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('scientia-mobile/mdp-uploader');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['S3Uploader'] = $this->app->share(function ($app) {

			$config = $app['config']['mdp-uploader'] ?: $app['config']['mdp-uploader::config'];

			if (isset($config['aws'])) {
				$aws_config = $config['aws'];
			}
      if (isset($config['aws']['config_file'])) {
          $aws_config = $config['config_file'];
      }

			return new S3Uploader($config, $aws_config);
		});

		$this->app['FileUploader'] = $this->app->share(function ($app) {
			$config = $app['config']['mdp-uploader'] ?: $app['config']['mdp-uploader::config'];
			return new FileUploader($config);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}