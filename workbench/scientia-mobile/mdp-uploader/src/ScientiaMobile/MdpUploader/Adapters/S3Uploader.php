<?php namespace ScientiaMobile\MdpUploader\Adapters;

use Aws\Common\Enum\Size;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;


class S3Uploader extends GenericUploader {

  public function __construct(array $config, array $aws_config)
  {
    $this->config = $config;
    $this->registerS3($aws_config);
  }

  public function upload(UploadFile $file)
  {
    // Inherit parent function, as needed
    parent::upload($file);

    $uploader = UploadBuilder::newInstance()
        ->setClient($this->getS3Client())
        ->setSource('/path/to/large/file.mov')
        ->setBucket('mybucket')
        ->setKey('my-object-key')
        ->setOption('Metadata', array('Foo' => 'Bar'))
        ->setOption('CacheControl', 'max-age=3600')
        ->build();

    // Perform the upload. Abort the upload if something goes wrong
    try {
      $uploader->upload();
      echo "Upload complete.\n";
    } catch (MultipartUploadException $e) {
      $uploader->abort();
      echo "Upload failed.\n";
    }
    
    //return array($this->cleanPath(static::$app['config']->get('cabinet::upload_folder')).$this->dateFolderPath, $file->fileSystemName);
  }

}