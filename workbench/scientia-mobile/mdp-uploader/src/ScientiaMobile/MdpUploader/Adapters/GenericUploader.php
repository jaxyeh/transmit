<?php namespace ScientiaMobile\MdpUploader\Adapters;

use ScientiaMobile\Streamer\Streamer;

class GenericUploader extends Streamer {

  protected $uploadFile;

  protected $config = array(
    'upload_folder' => '',
  );

  public function __construct(array $config)
  {
    // NOTE: Need to be very careful, what happen if $config array is empty?
    $this->config = $config;
  }

  public function process(UploadFile $file)
  {

    $this->upload($file);

  }



}