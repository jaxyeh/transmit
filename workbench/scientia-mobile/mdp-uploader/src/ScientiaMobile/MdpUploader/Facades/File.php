<?php namespace ScientiaMobile\MdpUploader\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * UploaderS3 binding for Laravel 4 applications
 */

class File extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'FileUploader';
    }
}